# 可抖视App

#### 主页
[网站介绍](https://nasio-limbo.github.io/sights.html)

#### 介绍
「应用简介」

可抖视是一款非常实用的短视频下载应用，可帮助用户能够轻松下载短视频，便于能够更加高效的观看，看视频更加的轻松方便。


「功能介绍」

视频下载：支持多个抖音短视频的去水印视频下载


「必要说明」

本软件禁止用于非法活动，请勿下载受版权保护文件，所有下载行为均由用户自行负责。


#### 安装
[华为商店](https://appgallery.huawei.com/#/app/C104547873)

[直链](https://panpic.bilnn.com/api/v3/file/sourcejump/QPgmk8tb/J_vww8q_XvuNDBO1wsRnaLMnNBHZfBvVAvqxPW0uUcA*)

#### 预览
<div  align="center">
    <img src="Screenshots/1.png" width="375" height="666"/><br/>    
    <img src="Screenshots/2.png" width="375" height="666"/><br/>
    <img src="Screenshots/3.png" width="375" height="666"/><br/>
    <img src="Screenshots/4.png" width="375" height="666"/><br/>
</div>
